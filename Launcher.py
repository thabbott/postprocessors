import PPConstants
from PostProcessor import PostProcessor

class Launcher(object):

    def __init__(self):
        """
        Initialize a Launcher
        """
        self.tasks = [] # Will be filled with PostProcessors

    def setup(self):
        """
        Initialize the Launcher by parsing PostProcessors from
        the configuration file
        """
        try:
            with open(PPConstants.CONFIG, 'r') as config:
                self.parse(config)
        except IOError:
            print "Could not find configuration file"
            print "Check that PostProcessors.config exists"

    def launch(self):
        """
        Launch the postprocessing tasks
        """
        for p in self.tasks:
            p.run()

    def status_report(self):
        """
        Report the status of postprocessing tasks
        """
        total = len(self.tasks)
        nr = 0 # Never ran
        fe = 0 # Didn't run because file exists
        s = 0 # Suceeded
        f = 0 # Failed
        for p in self.tasks:
            if p.status == PPConstants.NEVER_STARTED:
                nr += 1
            if p.status == PPConstants.FILE_EXISTS:
                fe += 1
            if p.status == PPConstants.SUCCESS:
                s += 1
            if p.status == PPConstants.FAIL:
                f += 1
        print "Out of %d tasks," % total
        print "%d did not fail unexpectedly (%.1f %%)"\
            % (s + fe, 100*float(s + fe)/total)
        print "%d ran and succeeded (%.1f %%)"\
            % (s, 100*float(s)/total)
        print "%d did not run because of existing output (%.1f %%)"\
            % (fe, 100*float(fe)/total)
        print "%d ran and failed unexpectedly (%.1f %%)"\
            % (f, 100*float(f)/total)
        print "%d did not start (%.1f %%)"\
            % (nr, 100*float(nr)/total)

    def swarm(self):
        """
        Create a submission script and a set of python scripts to run
        postprocessing tasks as a SLURM swarm
        """
        pass
            
    def parse(self, config):
        """
        Parse a configuration file and create the list of PostProcessors

        Parameters
        ----------
        config: file object
            file object that gives read access to the configuration file
        """
        config.seek(0,0)
        inprog = False
        lnum = 1
        while True:
            line = config.readline()
            # End of file
            if self.match(line, PPConstants.EOF):
                break

            # Blank line
            if self.match(line, PPConstants.BLANK_LINE):
                lnum = lnum + 1
                continue

            # Comment
            if self.match(line, PPConstants.COMMENT):
                lnum = lnum + 1
                continue

            # Block start
            if self.match(line, PPConstants.BLOCK_START):
                if inprog:
                    self.error("START found in middle of block", lnum)
                else:
                    inprog = True
                    func = config.readline()
                    fname = config.readline()
                    if self.match(func, PPConstants.EOF):
                        self.error("EOF in middle of block", lnum+1)
                    if self.match(func, PPConstants.BLOCK_END):
                        self.error("END in middle of block", lnum+1)
                    if self.match(fname, PPConstants.EOF):
                        self.error("EOF in middle of block", lnum+2)
                    if self.match(fname, PPConstants.BLOCK_END):
                        self.error("END in middle of block", lnum+2)
                    lnum = lnum + 3
                    params = {}
                    line = config.readline()
                    while not self.match(line, PPConstants.BLOCK_END):
                        line = line.strip().split(':')
                        if not len(line) == 2:
                            self.error("Invalid parameter specification", lnum)
                        if line[0].strip() in params.keys():
                            self.error(
                               "Duplicate parameter: {}".\
                                   format(line[0].strip()),
                               lnum)
                            pass
                        params[line[0].strip()] = line[1].strip()
                        line = config.readline()
                        lnum = lnum + 1
                    inprog = False
                    self.tasks.insert(0, PostProcessor(
                        func.strip(), fname.strip(), params))
                    continue

            # Block end
            if self.match(line, PPConstants.BLOCK_END):
               if not inprog:
                   self.error("END found outside of block", lnum)
               lnum = lnum + 1
               continue

            # Raise error
            self.error("Unrecognized statement", lnum)

        # Check for appropriate exit
        if inprog:
            self.error("Parsing finished inside block", -1)

    def match(self, source, target):
        """
        String matcher for parser

        Parameters
        ----------
        source: string
            string being tested

        target: string
            test target

        Returns
        -------
        bool: True if source matches target, False otherwise
        """
        # Special logic for EOF
        if target == PPConstants.EOF:
            if len(source) > 0:
                return False
            return True
        if target == PPConstants.BLANK_LINE:
            if len(source.strip()) == 0:
                return True
            return False
        return source.lower().strip().startswith(target)

    def error(self, message, lnum):
        """
        Error message generator for parser

        Parameters
        ----------
        message: string
            error message

        lnum: int
            line number at which error occured
        """
        print "Error: PostProcessors.parse"
        print message
        print "Line: %d" % lnum
        exit()
    

