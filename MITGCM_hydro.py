"""
Postprocessing functions for MITGCM runs related to the hydrological cycle
"""

import numpy as np
import h5py
import griddata
import streaming
import mds
import uv_cs
import time

def cs_zonal_hydro_cycle(fname, params):
    """
    Compute zonal mean statistics related to the hydrological cycle,
    including mass sources and sinks.

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
        'file_mass': string
            File containing mass source diagnostic
        'recnum_mass': string
            Record number of mass source diagnostic
        'file_eta': string
            File containing surface pressure deviation diagnostic
        'recnum_eta': string
            Record number of surface pressure deviation diagnostic
    """
    print 'Entering MITGCM_hydro.cs_zonal_hydro_cycle'
    t = time.time()
    print 'Loading variables'
    
    # Construct list of iterations
    iters = np.arange(int(params['iter0']),
            int(params['iterN']) + int(params['iterS']),
            int(params['iterS']))

    # Construct grid
    xci = np.linspace(float(params['xi0']), float(params['xiN']),
            int(params['nx']))
    yci = np.linspace(float(params['yi0']), float(params['yiN']),
            int(params['ny']))
    xci, yci = np.meshgrid(xci, yci)
    
    # Load variables
    ddir = params['ddir']
    vnm = params['file_mass']
    recm = int(params['recnum_mass'])
    vne = params['file_eta']
    rece = int(params['recnum_eta'])
    M = mds.rdmds('%s/%s' % (ddir, vnm), itrs = iters, 
        rec = [recm], returnmeta = False)
    Eta = mds.rdmds('%s/%s' % (ddir, vne), itrs = iters,
        rec = [rece], returnmeta = False)
    if len(iters) == 1:
        M = M[np.newaxis, :, :, :]
        Eta = Eta[np.newaxis,:,:]
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    rac = mds.rdmds('%s/RAC' % ddir, returnmeta = False)
    drf = mds.rdmds('%s/DRF' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)
    if len(np.unique(rosurf)) != 1:
        raise ValueError('Topography present in run!')
    rosurf = rosurf.ravel()[0]
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Rotating and interpolating'

    # Convert to vertical distribution of hydrostatic pressure tendency
    # Units 1/day
    for ii in range(M.shape[0]):
        for jj in range(M.shape[1]):
            M[ii,jj,:,:] = 86400. * \
                ((rosurf + Eta[ii,:,:].squeeze()) / rosurf) * \
                M[ii,jj,:,:].squeeze() / (9.80*rac*drf[jj])
    
    # Compute triangulation
    dela = griddata.linearp(xc, yc, xci, yci, wrap = [-180, 180, 5])

    # Interpolate
    Mi = np.zeros(M.shape[:-2] + (xci.shape[0], xci.shape[1]))
    Psurfi = np.zeros((Eta.shape[0], xci.shape[0], xci.shape[1]))
    for ii in range(M.shape[0]):
        Psurfi[ii,:,:] = rosurf + \
            griddata.lineari(dela, Eta[ii,:,:].squeeze())
        for jj in range(M.shape[1]):
            Mi[ii,jj,:,:] = griddata.lineari(dela, M[ii,jj,:,:].squeeze())

    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Averaging (over %d snapshots)' % len(iters)

    # Average in time and longitude, weighted by surface pressure
    Mz = np.zeros((xci.shape[0], M.shape[1]))
    Psurfz = np.sum(Psurfi, axis = (0,2))
    for jj in range(M.shape[1]):
        Mz[:,jj] = np.transpose(
            np.sum(Psurfi*Mi[:,jj,:,:].squeeze(), axis = (0,2)) / \
            Psurfz)

    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Computing eddy quantities'

    # Compute deviation temperature and velocities
    # Mp = np.zeros(Mi.shape)
    # for ii in range(Mp.shape[0]):
    #     for jj in range(Mp.shape[2]):
    #         Mp[ii,:,:,jj] = Mi[ii,:,:,jj].squeeze() - np.transpose(Mz)
    # 
    # print 'Elapsed time: %.2f s' % ((time.time() - t))
    # t = time.time()
    # print 'Averaging (over %d snapshots)' % len(iters)

    # # Average in time and longitude, weighted by surface pressure
    # Mvar = np.zeros(Mz.shape)
    # for jj in range(M.shape[1]):
    #     Mvar[:,jj] = np.transpose(
    #         np.sum(Psurfi*Mp[:,jj,:,:].squeeze()**2, axis = (0,2)) / \
    #         Psurfz)

    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Saving'
    
    # Load vertical coordinate
    sigma = mds.rdmds('%s/RC' % params['ddir'])/rosurf

    # Compute zonal mean surface pressure
    Psurfz = Psurfz/(Psurfi.shape[0]*Psurfi.shape[2])

    # Save fields
    f = h5py.File(fname, 'w')

    f['M'] = Mz
    f['M'].attrs['units'] = '1/day'
    f['M'].attrs['longname'] = 'Vertical Mass Source Distribution'
    
    f['sigma'] = sigma.squeeze()
    f['sigma'].attrs['units'] = 'nondim'
    f['sigma'].attrs['longname'] = 'sigma'

    f['lat'] = yci[:,0].squeeze()
    f['lat'].attrs['units'] = 'degrees'
    f['lat'].attrs['longname'] = 'latitude'

    f['M'].dims.create_scale(f['lat'], 'lat')
    f['M'].dims[0].attach_scale(f['lat'])
    f['M'].dims[0].label = 'lat'
    f['M'].dims.create_scale(f['sigma'], 'sigma')
    f['M'].dims[1].attach_scale(f['sigma'])
    f['M'].dims[1].label = 'sigma'

    f['ps'] = Psurfz
    f['ps'].attrs['units'] = 'Pa'
    f['ps'].attrs['longname'] = 'Zonal Mean Surface Pressure'
    f['ps'].dims.create_scale(f['lat'], 'lat')
    f['ps'].dims[0].attach_scale(f['lat'])
    f['ps'].dims[0].label = 'lat'
    
    f.close()
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))   
    print 'Leaving MITGCM_hydro.cs_zonal_hydro_cycle'
    print

def cs_zonal_surface_loading(fname, params):
    """
    Compute zonal mean surface loading (pressure) from moisture
    and dry air.

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
    """

    print 'Entering MITGCM_hydro.cs_zonal_surface_loading'
    t = time.time()
    print 'Setting up storage'
    
    # Construct list of iterations
    ddir = params['ddir']
    iters = np.arange(int(params['iter0']),
            int(params['iterN']) + int(params['iterS']),
            int(params['iterS']))

    # Determine number of repetitions to load iterations 100 at once
    nr = 0
    ii = 0
    subsize = 100
    while ii < len(iters):
        nr+=1
        ii+=subsize

    # Construct grid
    xci = np.linspace(float(params['xi0']), float(params['xiN']),
            int(params['nx']))
    yci = np.linspace(float(params['yi0']), float(params['yiN']),
            int(params['ny']))
    xci, yci = np.meshgrid(xci, yci)

    # Construct arrays to hold fields
    Psurfz = np.zeros(xci.shape[0])
    Psurfzq = np.zeros(xci.shape[0])
     
    # Load time-independent fields 
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    drf = mds.rdmds('%s/DRF' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)
    if len(np.unique(rosurf)) != 1:
        raise ValueError('Topography present in run!')
    rosurf = rosurf.ravel()[0]
        
    # Compute triangulation
    dela = griddata.linearp(xc, yc, xci, yci, wrap = [-180, 180, 5])

    # Loop over subsets of snapshots and compute averages
    for kk in range(nr):
        if subsize*(kk+1) < len(iters):
            itc = iters[(subsize*kk):(subsize*(kk+1))]
        else:
            itc = iters[(subsize*kk):]

        print 'Elapsed time: %d s' % (time.time() - t)
        t = time.time()
        print 'Starting subset %d of %d' % (kk+1, nr)
        print 'Loading variables'
        
        # Load variables
        S = mds.rdmds('%s/%s' % (ddir, 'S'), itrs = itc, returnmeta = False)
        Eta = mds.rdmds('%s/%s' %  (ddir, 'Eta'), itrs = itc,
            returnmeta = False)
        if len(itc) == 1:
            S = S[np.newaxis, :, :, :]
            Eta = Eta[np.newaxis,:,:]

        print 'Elapsed time: %.2f s' % ((time.time() - t))
        t = time.time()
        print 'Updating zonal averages'

        # Interpolate and update zonal average surface pressure
        Psurfi = np.zeros((S.shape[0], xci.shape[0], xci.shape[1]))
        for ii in range(S.shape[0]):
            Psurfi[ii,:,:] = rosurf + \
                griddata.lineari(dela, Eta[ii,:,:].squeeze())
        Psurfz = Psurfz + np.sum(Psurfi, axis = (0,2))
        
        # Compute fraction of mass in each column that is moist
        for jj in range(S.shape[1]):
            S[:,jj,:,:] = S[:,jj,:,:]*drf[jj]
        mfrac = np.sum(S, axis = 1)/np.sum(drf)
        
        # Interpolate and update moist component
        Psurfi = np.zeros((S.shape[0], xci.shape[0], xci.shape[1]))
        for ii in range(S.shape[0]):
            Psurfi[ii,:,:] = griddata.lineari(dela, \
                mfrac[ii,:,:].squeeze()*(rosurf+Eta[ii,:,:].squeeze()))
        Psurfzq = Psurfzq + np.sum(Psurfi, axis = (0,2))
    
    # END for kk in range(nr)
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Final averaging'

    # Finish averaging
    Psurfz = Psurfz/(len(iters)*Psurfi.shape[2])
    Psurfzq = Psurfzq/(len(iters)*Psurfi.shape[2])
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Saving'

    # Save fields
    f = h5py.File(fname, 'w')

    f['lat'] = yci[:,0].squeeze()
    f['lat'].attrs['units'] = 'degrees'
    f['lat'].attrs['longname'] = 'latitude'

    f['ps'] = Psurfz
    f['ps'].attrs['units'] = 'Pa'
    f['ps'].attrs['longname'] = 'Zonal Mean Surface Pressure'
    f['ps'].dims.create_scale(f['lat'], 'lat')
    f['ps'].dims[0].attach_scale(f['lat'])
    f['ps'].dims[0].label = 'lat'
    
    f['psq'] = Psurfzq
    f['psq'].attrs['units'] = 'Pa'
    f['psq'].attrs['longname'] = 'Water Vapor Component of Surface Pressure'
    f['psq'].dims.create_scale(f['lat'], 'lat')
    f['psq'].dims[0].attach_scale(f['lat'])
    f['psq'].dims[0].label = 'lat'
    
    f.close()
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))   
    print 'Leaving MITGCM_hydro.cs_zonal_surface_loading'
    print
