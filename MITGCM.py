"""
Postprocessing functions for MITGCM runs
"""

import numpy as np
import h5py
import griddata
import streaming
import mds
import uv_cs

def interpolator(it, ddir, vname, xci, yci):
    """
    Interpolate a scalar field on a cube sphere grid onto a lat-lon
    grid and return it. To interpolate a vector field, use interpolator_uv

    This function assumes that scalar fields are located at XC, YC, and RC
    (see MITGCM documentation for staggered grid details) and that XC
    is between -180 and 180 degrees

    Note that a typical use in functions from the streaming utility
    is through an anonymous function
        streaming.zonal_mean_ll((lamba i: interpolator(i, ...)), ...)

    Parameters
    ----------
    it: int
        Iteration number for which to perform the interpolation

    ddir: string
        Path to directory containing output files

    vname: string
        Name of the variable being interpolated

    xci: array-like
        Longitude of interpolating grid points

    yci: array-like
        Latitude of interpolating grid points

    Returns
    -------
    array-like
        Variable interpolated onto lat-lon grid
    """
    # Load variables
    V = mds.rdmds('%s/%s' % (ddir, vname), itrs = it, returnmeta = False)
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)

    # Compute triangulation
    dela = griddata.linearp(xc, yc, xci, yci, wrap = [-180, 180, 5])

    # Interpolate
    Vi = np.zeros([V.shape[0], xci.shape[0], xci.shape[1]])
    for ii in np.arange(V.shape[0]):
        Vit = griddata.lineari(dela, V[ii,:,:].squeeze())
        Vi[ii,:,:] = Vit
    return Vi

def interpolator_diag(it, ddir, vname, recnum, xci, yci):
    """
    Interpolate a diagnostic field on a cube sphere grid onto a lat-lon
    grid and return it. To interpolate a vector field, use interpolator_uv,
    and to interpolate a scalar snapshot, use interpolator.

    This function assumes that the field is located at XC, YC, and RC
    (see MITGCM documentation for staggered grid details) and that XC
    is between -180 and 180 degrees

    Note that a typical use in functions from the streaming utility
    is through an anonymous function
        streaming.zonal_mean_ll((lamba i: interpolator_diag(i, ...)), ...)

    Parameters
    ----------
    it: int
        Iteration number for which to perform the interpolation

    ddir: string
        Path to directory containing output files

    vname: string
        Name of the variable being interpolated

    recnum: int
        Record number of the diagnostic being interpolated

    xci: array-like
        Longitude of interpolating grid points

    yci: array-like
        Latitude of interpolating grid points

    Returns
    -------
    array-like
        Variable interpolated onto lat-lon grid
    """
    # Load variables
    V = mds.rdmds('%s/%s' % (ddir, vname), itrs = it, 
        rec = [recnum], returnmeta = False)
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)

    # Compute triangulation
    dela = griddata.linearp(xc, yc, xci, yci, wrap = [-180, 180, 5])

    # Interpolate
    Vi = np.zeros([V.shape[0], xci.shape[0], xci.shape[1]])
    for ii in np.arange(V.shape[0]):
        Vit = griddata.lineari(dela, V[ii,:,:].squeeze())
        Vi[ii,:,:] = Vit
    return Vi

def diag_fields(ddir, vname):
    """
    Return a list of diagnostic fields
    
    Parameters
    ----------
    ddir: string
        Path to directory containing output file

    vname: string
        Output file name

    Returns
    -------
    list of string:
        diagnostic field names
    """
    _,_,meta = mds.rdmds('%s/%s' % (ddir, vname), 
        itrs = np.inf, returnmeta = True)
    return meta['fldlist']


def interpolator_uv(it, ddir, uname, vname, xci, yci):
    """
    Interpolate a horizontal field on a cube sphere grid onto a lat-lon
    grid and return it. To interpolate a scalar field, use interpolator

    This function assumes that vector fields are located on cell faces
    between XG grid points and that X runs between -180 and 180 degrees.
    The fields are averaged onto XC and YC before rotating and interpolating.

    Note that a typical use in functions from the streaming utility
    is through an anonymous function
        streaming.zonal_mean_ll((lamba i: interpolator_uv(i, ...)), ...)

    Parameters
    ----------
    it: int
        Iteration number for which to perform the interpolation

    ddir: string
        Path to directory containing output files

    vname: string
        Name of the v-component variable being interpolated
    
    uname: string
        Name of the u-component variable being interpolated

    xci: array-like
        Longitude of interpolating grid points

    yci: array-like
        Latitude of interpolating grid points

    Returns
    -------
    array-like
        Variable interpolated onto lat-lon grid
    """
    # Load variables
    U = mds.rdmds('%s/%s' % (ddir, uname), itrs = it, returnmeta = False)
    V = mds.rdmds('%s/%s' % (ddir, vname), itrs = it, returnmeta = False)
    AngleCS = mds.rdmds('%s/AngleCS' % ddir, returnmeta = False)
    AngleSN = mds.rdmds('%s/AngleSN' % ddir, returnmeta = False)
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)

    # Average and rotate
    (U,V) = uv_cs.rotate_uv2uvEN(U, V, AngleCS, AngleSN)

    # Compute triangulation
    dela = griddata.linearp(xc, yc, xci, yci, wrap = [-180, 180, 5])

    # Interpolate
    Ui = np.zeros([U.shape[0], xci.shape[0], xci.shape[1]])
    for ii in np.arange(U.shape[0]):
        Ui[ii,:,:] = griddata.lineari(dela, U[ii,:,:].squeeze())
    Vi = np.zeros([V.shape[0], xci.shape[0], xci.shape[1]])
    for ii in np.arange(V.shape[0]):
        Vi[ii,:,:] = griddata.lineari(dela, V[ii,:,:].squeeze())
    return (Ui,Vi)

def cs_zonal_theta(fname, params):
    """
    Compute zonal mean potential temperature

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
        'p0': float
            Reference surface pressure for sigma coordinates
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Construct grid
    xi = np.linspace(float(params['xi0']), float(params['xiN']),
            int(params['nx']))
    yi = np.linspace(float(params['yi0']), float(params['yiN']),
            int(params['ny']))
    xi, yi = np.meshgrid(xi, yi)

    # Compute zonal mean
    T = streaming.zonal_nanmean_ll(
            (lambda i: interpolator( i,
                                    params['ddir'],
                                    'T',
                                    xi, yi
                                  )),
            its = iters).transpose()
    sigma = mds.rdmds('%s/RC' % params['ddir'])/float(params['p0'])

    # Save
    f = h5py.File(fname, 'w')
    f['theta'] = T
    f['theta'].attrs['units'] = 'K'
    f['theta'].attrs['longname'] = 'Potential Temperature'
    
    f['sigma'] = sigma.squeeze()
    f['sigma'].attrs['units'] = 'nondim'
    f['sigma'].attrs['longname'] = 'sigma'

    f['lat'] = yi[:,0].squeeze()
    f['lat'].attrs['units'] = 'degrees'
    f['lat'].attrs['longname'] = 'latitude'

    f['theta'].dims.create_scale(f['lat'], 'lat')
    f['theta'].dims[0].attach_scale(f['lat'])
    f['theta'].dims[0].label = 'lat'
    f['theta'].dims.create_scale(f['sigma'], 'sigma')
    f['theta'].dims[1].attach_scale(f['sigma'])
    f['theta'].dims[1].label = 'sigma'

    f.close()

def cs_zonal_uv(fname, params):
    """
    Compute zonal mean zonal and meridional wind

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
        'p0': float
            Reference surface pressure for sigma coordinates
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Construct grid
    xi = np.linspace(float(params['xi0']), float(params['xiN']),
            int(params['nx']))
    yi = np.linspace(float(params['yi0']), float(params['yiN']),
            int(params['ny']))
    xi, yi = np.meshgrid(xi, yi)

    # Compute zonal mean
    U = streaming.zonal_nanmean_ll(
            (lambda i: interpolator_uv( i,
                                    params['ddir'],
                                    'U', 'V',
                                    xi, yi
                                  )[0]),
            its = iters).transpose()
    V = streaming.zonal_nanmean_ll(
            (lambda i: interpolator_uv( i,
                                    params['ddir'],
                                    'U', 'V',
                                    xi, yi
                                  )[1]),
            its = iters).transpose()
    sigma = mds.rdmds('%s/RC' % params['ddir'])/float(params['p0'])

    # Save
    f = h5py.File(fname, 'w')
    f['U'] = U
    f['V'] = V
    f['U'].attrs['units'] = 'm/s'
    f['V'].attrs['units'] = 'm/s'
    f['U'].attrs['longname'] = 'Zonal mean zonal wind'
    f['V'].attrs['longname'] = 'Zonal mean meridional wind'
    
    f['sigma'] = sigma.squeeze()
    f['sigma'].attrs['units'] = 'nondim'
    f['sigma'].attrs['longname'] = 'sigma'

    f['lat'] = yi[:,0].squeeze()
    f['lat'].attrs['units'] = 'degrees'
    f['lat'].attrs['longname'] = 'latitude'

    f['U'].dims.create_scale(f['lat'], 'lat')
    f['U'].dims[0].attach_scale(f['lat'])
    f['U'].dims[0].label = 'lat'
    f['U'].dims.create_scale(f['sigma'], 'sigma')
    f['U'].dims[1].attach_scale(f['sigma'])
    f['U'].dims[1].label = 'sigma'
    
    f['V'].dims.create_scale(f['lat'], 'lat')
    f['V'].dims[0].attach_scale(f['lat'])
    f['V'].dims[0].label = 'lat'
    f['V'].dims.create_scale(f['sigma'], 'sigma')
    f['V'].dims[1].attach_scale(f['sigma'])
    f['V'].dims[1].label = 'sigma'

    f.close()


def interpolator_mass(it, ddir, vname, recnum, xci, yci):
    """
    Interpolate a mass field on a cube sphere grid onto a lat-lon
    grid after converting the field into hydrostatic pressure units.

    This function assumes that the field is located at XC, YC, and RC
    (see MITGCM documentation for staggered grid details) and that XC
    is between -180 and 180 degrees

    Note that a typical use in functions from the streaming utility
    is through an anonymous function
        streaming.zonal_mean_ll((lamba i: interpolator_mass(i, ...)), ...)

    Parameters
    ----------
    it: int
        Iteration number for which to perform the interpolation

    ddir: string
        Path to directory containing output files

    vname: string
        Name of the variable being interpolated

    recnum: int
        Record number of the diagnostic being interpolated

    xci: array-like
        Longitude of interpolating grid points

    yci: array-like
        Latitude of interpolating grid points

    Returns
    -------
    array-like
        Variable interpolated onto lat-lon grid
    """
    # Load variables
    V = mds.rdmds('%s/%s' % (ddir, vname), itrs = it, 
        rec = [recnum], returnmeta = False)
    Eta = mds.rdmds('%s/surfDiag' % (ddir), itrs = it,
        rec = [0], returnmeta = False)
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    rac = mds.rdmds('%s/RAC' % ddir, returnmeta = False)
    drf = mds.rdmds('%s/DRF' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)

    # Convert to vertical distribution of hydrostatic pressure tendency
    # Units 1/day
    for ii in range(V.shape[0]):
        V[ii,:,:] = 86400. * ((rosurf + Eta) / rosurf) * \
                    V[ii,:,:] / (9.80*rac*drf[ii])

    # Compute triangulation
    dela = griddata.linearp(xc, yc, xci, yci, wrap = [-180, 180, 5])

    # Interpolate
    Vi = np.zeros([V.shape[0], xci.shape[0], xci.shape[1]])
    for ii in np.arange(V.shape[0]):
        Vi[ii,:,:] = griddata.lineari(dela, V[ii,:,:].squeeze())
    return Vi

def cs_zonal_massdiag(fname, params):
    """
    Compute the zonal mean mass tendency from diagnostic output

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'dname': int
            Diagnostics file name
        'recnum': string
            Name of the diagnostic field
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
        'p0': float
            Reference surface pressure for sigma coordinates
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Construct grid
    xi = np.linspace(float(params['xi0']), float(params['xiN']),
            int(params['nx']))
    yi = np.linspace(float(params['yi0']), float(params['yiN']),
            int(params['ny']))
    xi, yi = np.meshgrid(xi, yi)

    # Get diagnostic metadata
    dfields = diag_fields(params['ddir'], params['dname'])

    # Compute zonal mean
    D = streaming.zonal_nanmean_ll(
            (lambda i: interpolator_mass( i,
                                    params['ddir'],
                                    params['dname'],
                                    int(params['recnum']),
                                    xi, yi
                                  )),
            its = iters).transpose()
    sigma = mds.rdmds('%s/RC' % params['ddir'])/float(params['p0'])

    # Save
    f = h5py.File(fname, 'w')
    f['mtend'] = D
    f['mtend'].attrs['units'] = '1/day'
    f['mtend'].attrs['longname'] = 'Mass tendency'
    
    f['sigma'] = sigma.squeeze()
    f['sigma'].attrs['units'] = 'nondim'
    f['sigma'].attrs['longname'] = 'sigma'

    f['lat'] = yi[:,0].squeeze()
    f['lat'].attrs['units'] = 'degrees'
    f['lat'].attrs['longname'] = 'latitude'

    f['mtend'].dims.create_scale(f['lat'], 'lat')
    f['mtend'].dims[0].attach_scale(f['lat'])
    f['mtend'].dims[0].label = 'lat'
    f['mtend'].dims.create_scale(f['sigma'], 'sigma')
    f['mtend'].dims[1].attach_scale(f['sigma'])
    f['mtend'].dims[1].label = 'sigma'

    f.close()

def cs_zonal_diag(fname, params):
    """
    Compute zonal mean of some diagnostic
    from diagnostic output.

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'longname': string
            Descriptive name of diagnostic variable
        'units': string
            Units of variable
        'dname': int
            Diagnostics file name
        'recnum': string
            Name of the diagnostic field
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
        'p0': float
            Reference surface pressure for sigma coordinates
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Construct grid
    xi = np.linspace(float(params['xi0']), float(params['xiN']),
            int(params['nx']))
    yi = np.linspace(float(params['yi0']), float(params['yiN']),
            int(params['ny']))
    xi, yi = np.meshgrid(xi, yi)

    # Get diagnostic metadata
    dfields = diag_fields(params['ddir'], params['dname'])
    key = dfields[int(params['recnum'])]

    # Compute zonal mean
    D = streaming.zonal_nanmean_ll(
            (lambda i: interpolator_diag( i,
                                    params['ddir'],
                                    params['dname'],
                                    int(params['recnum']),
                                    xi, yi
                                  )),
            its = iters).transpose()
    sigma = mds.rdmds('%s/RC' % params['ddir'])/float(params['p0'])

    # Save
    f = h5py.File(fname, 'w')
    f[key] = D
    f[key].attrs['units'] = params['units']
    f[key].attrs['longname'] = params['longname']
    
    f['sigma'] = sigma.squeeze()
    f['sigma'].attrs['units'] = 'nondim'
    f['sigma'].attrs['longname'] = 'sigma'

    f['lat'] = yi[:,0].squeeze()
    f['lat'].attrs['units'] = 'degrees'
    f['lat'].attrs['longname'] = 'latitude'

    f[key].dims.create_scale(f['lat'], 'lat')
    f[key].dims[0].attach_scale(f['lat'])
    f[key].dims[0].label = 'lat'
    f[key].dims.create_scale(f['sigma'], 'sigma')
    f[key].dims[1].attach_scale(f['sigma'])
    f[key].dims[1].label = 'sigma'

    f.close()

def total_mass(fname, params):
    """
    Compute a timeseries of the total atmospheric mass

    Parameters
    ----------
    fname: string
        Name of the output file where the timseries is stored
        The output file format will be HDF5

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Compute total mass
    mtot = np.zeros(iters.size)
    ddir = params['ddir']
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    rac = mds.rdmds('%s/RAC' % ddir, returnmeta = False)
    drf = mds.rdmds('%s/DRF' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)
    ii = 0
    for it in iters:
        # Get surface pressure (stored in Eta at end)
        Eta = mds.rdmds('%s/Eta' % (ddir), itrs = it, returnmeta = False)
        Eta = Eta + rosurf

        # Convert to mass and integrate over area
        mtot[ii] = np.sum(Eta*rac)/9.80
        ii+=1

    # Save
    f = h5py.File(fname, 'w')
    f['mass'] = mtot
    f['mass'].attrs['units'] = 'kg'
    f['mass'].attrs['longname'] = 'Total atmospheric mass'
    
    f['iter'] = iters
    f['iter'].attrs['units'] = 'nondim'
    f['iter'].attrs['longname'] = 'Iteration'

    f['mass'].dims.create_scale(f['iter'], 'iter')
    f['mass'].dims[0].attach_scale(f['iter'])
    f['mass'].dims[0].label = 'iter'

    f.close()

def dry_mass(fname, params):
    """
    Compute a timeseries of the dry atmospheric mass

    Parameters
    ----------
    fname: string
        Name of the output file where the timseries is stored
        The output file format will be HDF5

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Compute dry mass
    mtot = np.zeros(iters.size)
    ddir = params['ddir']
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    rac = mds.rdmds('%s/RAC' % ddir, returnmeta = False)
    drf = mds.rdmds('%s/DRF' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)
    ii = 0
    for it in iters:
        # Get surface pressure (stored in Eta at end)
        Eta = mds.rdmds('%s/Eta' % (ddir), itrs = it, returnmeta = False)
        Eta = Eta + rosurf
        
        # Compute fraction of mass in each column that is moist
        q = mds.rdmds('%s/S' % ddir, itrs = it, returnmeta = False)
        for jj in range(q.shape[0]):
            q[jj,:,:] = q[jj,:,:]*drf[jj]
        q = np.sum(q, axis=0)/np.sum(drf)
        
        # Convert to mass and integrate over area
        mtot[ii] = np.sum((1.-q)*Eta*rac)/9.80
        ii+=1

    # Save
    f = h5py.File(fname, 'w')
    f['mass'] = mtot
    f['mass'].attrs['units'] = 'kg'
    f['mass'].attrs['longname'] = 'Dry atmospheric mass'
    
    f['iter'] = iters
    f['iter'].attrs['units'] = 'nondim'
    f['iter'].attrs['longname'] = 'Iteration'

    f['mass'].dims.create_scale(f['iter'], 'iter')
    f['mass'].dims[0].attach_scale(f['iter'])
    f['mass'].dims[0].label = 'iter'

    f.close()
