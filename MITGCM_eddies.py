"""
Postprocessing functions for MITGCM runs related to eddy dynamics
"""

import numpy as np
import h5py
import griddata
import streaming
import mds
import uv_cs
import time

def cs_surface_pressure_pdf(fname, params):
    """
    Compute PDFs of surface pressure.

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
    """
    print 'Entering MITGCM_eddies.cs_surface_pressure_pdf'
    t = time.time()
    print 'Loading variables'
    
    # Construct list of iterations
    iters = np.arange(int(params['iter0']),
            int(params['iterN']) + int(params['iterS']),
            int(params['iterS']))
       
    # Load fields 
    ddir = params['ddir']
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)
    if len(np.unique(rosurf)) != 1:
        raise ValueError('Topography present in run!')
    rosurf = rosurf.ravel()[0]
    Eta = mds.rdmds('%s/%s' %  (ddir, 'Eta'), itrs = iters,
        returnmeta = False)
    if len(iters) == 1:
        Eta = Eta[np.newaxis,:,:]
    Eta = Eta + rosurf
        
    print 'Elapsed time: %d s' % (time.time() - t)
    t = time.time()
    print 'Computing PDFs'

    # Compute global PDF
    # Use Friedman-Draconis estimator for number of bins
    ot = 1./3.
    q75, q25 = np.percentile(Eta, [75, 25])
    iqr = q75 - q25
    bs = 2.*iqr/Eta.size**ot
    nbins = np.ceil((Eta.max() - Eta.min())/bs)
    p, e = np.histogram(Eta, bins = nbins, density = True)
    c = 0.5*(e[1:] + e[:-1])
    mu = np.mean(Eta)

    # Compute equatorial, mid-latitude, and polar PDFs
    mask = np.abs(np.tile(yc[np.newaxis,:,:], [Eta.shape[0], 1, 1])) < 30
    q75, q25 = np.percentile(Eta[mask], [75, 25])
    iqr = q75 - q25
    bs = 2.*iqr/np.sum(mask)**ot
    nbins = np.ceil((Eta[mask].max() - Eta[mask].min())/bs)
    peq, e = np.histogram(Eta[mask], bins = nbins, density = True)
    ceq = 0.5*(e[1:] + e[:-1])
    mueq = np.mean(Eta[mask])

    mask = (np.abs(np.tile(yc[np.newaxis,:,:], [Eta.shape[0], 1, 1])) >= 30) &\
        (np.abs(np.tile(yc[np.newaxis,:,:], [Eta.shape[0], 1, 1])) < 60.)
    q75, q25 = np.percentile(Eta[mask], [75, 25])
    iqr = q75 - q25
    bs = 2.*iqr/np.sum(mask)**ot
    nbins = np.ceil((Eta[mask].max() - Eta[mask].min())/bs)
    pmid, e = np.histogram(Eta[mask], bins = nbins, density = True)
    cmid = 0.5*(e[1:] + e[:-1])
    mumid = np.mean(Eta[mask])

    mask = np.abs(np.tile(yc[np.newaxis,:,:], [Eta.shape[0], 1, 1])) > 60
    q75, q25 = np.percentile(Eta[mask], [75, 25])
    iqr = q75 - q25
    bs = 2.*iqr/np.sum(mask)**ot
    nbins = np.ceil((Eta[mask].max() - Eta[mask].min())/bs)
    ppol, e = np.histogram(Eta[mask], bins = nbins, density = True)
    cpol = 0.5*(e[1:] + e[:-1])
    mupol = np.mean(Eta[mask])

    print 'Elapsed time: %d s' % (time.time() - t)
    t = time.time()
    print 'Saving'
    
    # Save fields
    f = h5py.File(fname, 'w')

    f['ps'] = c
    f['ps'].attrs['units'] = 'Pa'
    f['ps'].attrs['longname'] = 'Surface Pressure'
    
    f['pdps'] = p
    f['pdps'].attrs['units'] = '1/Pa'
    f['pdps'].attrs['longname'] = 'Probability Density'
    f['pdps'].attrs['mean'] = mu
    f['pdps'].dims.create_scale(f['ps'], 'ps')
    f['pdps'].dims[0].attach_scale(f['ps'])
    f['pdps'].dims[0].label = 'ps'
    
    f['ps_eq'] = ceq
    f['ps_eq'].attrs['units'] = 'Pa'
    f['ps_eq'].attrs['longname'] = 'Surface Pressure'
    
    f['pdps_eq'] = peq
    f['pdps_eq'].attrs['units'] = '1/Pa'
    f['pdps_eq'].attrs['longname'] = 'Probability Density (< 30 N/S)'
    f['pdps_eq'].attrs['mean'] = mueq
    f['pdps_eq'].dims.create_scale(f['ps_eq'], 'ps_eq')
    f['pdps_eq'].dims[0].attach_scale(f['ps_eq'])
    f['pdps_eq'].dims[0].label = 'ps_eq'
    
    f['ps_mid'] = cmid
    f['ps_mid'].attrs['units'] = 'Pa'
    f['ps_mid'].attrs['longname'] = 'Surface Pressure'
    
    f['pdps_mid'] = pmid
    f['pdps_mid'].attrs['units'] = '1/Pa'
    f['pdps_mid'].attrs['longname'] = 'Probability Density (30-60 N/S)'
    f['pdps_mid'].attrs['mean'] = mumid
    f['pdps_mid'].dims.create_scale(f['ps_mid'], 'ps_mid')
    f['pdps_mid'].dims[0].attach_scale(f['ps_mid'])
    f['pdps_mid'].dims[0].label = 'ps_mid'
    
    f['ps_pol'] = cpol
    f['ps_pol'].attrs['units'] = 'Pa'
    f['ps_pol'].attrs['longname'] = 'Surface Pressure'
    
    f['pdps_pol'] = ppol
    f['pdps_pol'].attrs['units'] = '1/Pa'
    f['pdps_pol'].attrs['longname'] = 'Probability Density (> 60 N/S)'
    f['pdps_pol'].attrs['mean'] = mupol
    f['pdps_pol'].dims.create_scale(f['ps_pol'], 'ps_pol')
    f['pdps_pol'].dims[0].attach_scale(f['ps_pol'])
    f['pdps_pol'].dims[0].label = 'ps_pol'

    f.close()
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))   
    print 'Leaving MITGCM_eddies.cs_surface_pressure_pdf'
    print

def cs_zonal_eddy_uvT(fname, params):
    """
    Compute zonal mean potential temperature, zonal wind,
    and meridional wind.

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
    """
    print 'Entering MITGCM_eddies.cs_zonal_eddy_uvT'
    t = time.time()
    print 'Setting up storage'
    
    # Construct list of iterations
    iters = np.arange(int(params['iter0']),
            int(params['iterN']) + int(params['iterS']),
            int(params['iterS']))

    # Determine number of repetitions to load iterations 100 at once
    nr = 0
    ii = 0
    subsize = 100
    while ii < len(iters):
        nr+=1
        ii+=subsize

    # Construct grid
    xci = np.linspace(float(params['xi0']), float(params['xiN']),
            int(params['nx']))
    yci = np.linspace(float(params['yi0']), float(params['yiN']),
            int(params['ny']))
    xci, yci = np.meshgrid(xci, yci)
    
    # Load a single snapshot to get dimensions
    ddir = params['ddir']
    U = mds.rdmds('%s/%s' % (ddir, 'U'), itrs = np.inf, returnmeta = False)

    # Construct arrays to hold fields
    Uz = np.zeros((xci.shape[0], U.shape[0]))
    Vz = np.zeros(Uz.shape)
    Tz = np.zeros(Uz.shape)
    Psurfz = np.zeros(Uz.shape[0])
    Uvar = np.zeros(Uz.shape)
    Vvar = np.zeros(Uz.shape)
    Tvar = np.zeros(Uz.shape)
    Pvar = np.zeros(Psurfz.shape)
    EKE = np.zeros(Uz.shape)
    EPTF = np.zeros(Uz.shape)
     
    # Load time-independent fields 
    AngleCS = mds.rdmds('%s/AngleCS' % ddir, returnmeta = False)
    AngleSN = mds.rdmds('%s/AngleSN' % ddir, returnmeta = False)
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)
    if len(np.unique(rosurf)) != 1:
        raise ValueError('Topography present in run!')
    rosurf = rosurf.ravel()[0]
        
    # Compute triangulation
    dela = griddata.linearp(xc, yc, xci, yci, wrap = [-180, 180, 5])

    # Loop over subsets of snapshots and compute averages
    for kk in range(nr):
        if subsize*(kk+1) < len(iters):
            itc = iters[(subsize*kk):(subsize*(kk+1))]
        else:
            itc = iters[(subsize*kk):]

        print 'Elapsed time: %d s' % (time.time() - t)
        t = time.time()
        print 'Starting subset %d of %d' % (kk+1, nr)
        print 'Loading variables'
        
        # Load variables
        U = mds.rdmds('%s/%s' % (ddir, 'U'), itrs = itc, returnmeta = False)
        V = mds.rdmds('%s/%s' % (ddir, 'V'), itrs = itc, returnmeta = False)
        T = mds.rdmds('%s/%s' % (ddir, 'T'), itrs = itc, returnmeta = False)
        Eta = mds.rdmds('%s/%s' %  (ddir, 'Eta'), itrs = itc,
            returnmeta = False)
        if len(itc) == 1:
            U = U[np.newaxis, :, :, :]
            V = V[np.newaxis, :, :, :]
            T = T[np.newaxis, :, :, :]
            Eta = Eta[np.newaxis,:,:]
        print 'Elapsed time: %.2f s' % ((time.time() - t))
        t = time.time()
        print 'Rotating and interpolating'

        # Average and rotate
        (U,V) = uv_cs.rotate_uv2uvEN(U, V, AngleCS, AngleSN)

        # Interpolate
        Ui = np.zeros(U.shape[:-2] + (xci.shape[0], xci.shape[1]))
        Vi = np.zeros(Ui.shape)
        Ti = np.zeros(Ui.shape)
        Psurfi = np.zeros((U.shape[0], xci.shape[0], xci.shape[1]))
        for ii in range(U.shape[0]):
            Psurfi[ii,:,:] = rosurf + \
                griddata.lineari(dela, Eta[ii,:,:].squeeze())
            for jj in range(U.shape[1]):
                Ui[ii,jj,:,:] = griddata.lineari(dela, U[ii,jj,:,:].squeeze())
                Vi[ii,jj,:,:] = griddata.lineari(dela, V[ii,jj,:,:].squeeze())
                Ti[ii,jj,:,:] = griddata.lineari(dela, T[ii,jj,:,:].squeeze())

        print 'Elapsed time: %.2f s' % ((time.time() - t))
        t = time.time()
        print 'Averaging (over %d snapshots)' % len(itc)

        # Average in time and longitude, weighted by surface pressure
        Psurfz = Psurfz + np.sum(Psurfi, axis = (0,2))
        Up = np.zeros(Ui.shape)
        Vp = np.zeros(Ui.shape)
        Tp = np.zeros(Ti.shape)
        for jj in range(U.shape[1]):
            Uz[:,jj] = Uz[:,jj] + np.transpose(
                np.sum(Psurfi*Ui[:,jj,:,:].squeeze(), axis = (0,2)))
            Vz[:,jj] = Vz[:,jj] + np.transpose(
                np.sum(Psurfi*Vi[:,jj,:,:].squeeze(), axis = (0,2)))
            Tz[:,jj] = Tz[:,jj] + np.transpose(
                np.sum(Psurfi*Ti[:,jj,:,:].squeeze(), axis = (0,2)))
    
    # END for kk in range(nr)
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Final averaging'

    # Finish averaging zonal means
    for jj in range(U.shape[1]):
        Uz[:,jj] = Uz[:,jj]/Psurfz
        Vz[:,jj] = Vz[:,jj]/Psurfz
        Tz[:,jj] = Tz[:,jj]/Psurfz

    # Loop over subsets of snapshots and compute eddy statistics
    for kk in range(nr):
        if subsize*(kk+1) < len(iters):
            itc = iters[(subsize*kk):(subsize*(kk+1))]
        else:
            itc = iters[(subsize*kk):]

        print 'Elapsed time: %d s' % (time.time() - t)
        t = time.time()
        print 'Starting subset %d of %d (eddy statistics)' % (kk+1, nr)
        print 'Loading variables'
        
        # Load variables
        U = mds.rdmds('%s/%s' % (ddir, 'U'), itrs = itc, returnmeta = False)
        V = mds.rdmds('%s/%s' % (ddir, 'V'), itrs = itc, returnmeta = False)
        T = mds.rdmds('%s/%s' % (ddir, 'T'), itrs = itc, returnmeta = False)
        Eta = mds.rdmds('%s/%s' %  (ddir, 'Eta'), itrs = itc,
            returnmeta = False)
        if len(itc) == 1:
            U = U[np.newaxis, :, :, :]
            V = V[np.newaxis, :, :, :]
            T = T[np.newaxis, :, :, :]
            Eta = Eta[np.newaxis,:,:]

        print 'Elapsed time: %.2f s' % ((time.time() - t))
        t = time.time()
        print 'Rotating and interpolating'

        # Average and rotate
        (U,V) = uv_cs.rotate_uv2uvEN(U, V, AngleCS, AngleSN)

        # Interpolate
        Ui = np.zeros(U.shape[:-2] + (xci.shape[0], xci.shape[1]))
        Vi = np.zeros(Ui.shape)
        Ti = np.zeros(Ui.shape)
        Psurfi = np.zeros((U.shape[0], xci.shape[0], xci.shape[1]))
        for ii in range(U.shape[0]):
            Psurfi[ii,:,:] = rosurf + \
                griddata.lineari(dela, Eta[ii,:,:].squeeze())
            for jj in range(U.shape[1]):
                Ui[ii,jj,:,:] = griddata.lineari(dela, U[ii,jj,:,:].squeeze())
                Vi[ii,jj,:,:] = griddata.lineari(dela, V[ii,jj,:,:].squeeze())
                Ti[ii,jj,:,:] = griddata.lineari(dela, T[ii,jj,:,:].squeeze())
        
        print 'Elapsed time: %.2f s' % ((time.time() - t))
        t = time.time()
        print 'Computing eddy quantities'

        # Compute deviation temperature, pressure, and velocities
        Up = np.zeros(Ui.shape)
        Vp = np.zeros(Ui.shape)
        Tp = np.zeros(Ti.shape)
        Pp = np.zeros(Psurfi.shape)
        for ii in range(Up.shape[0]):
            for jj in range(Up.shape[2]):
                Pp[ii,:,jj] = Psurfi[ii,:,jj].squeeze() - \
                    Psurfz/(len(iters)*Psurfi.shape[2])
                Up[ii,:,:,jj] = Ui[ii,:,:,jj].squeeze() - np.transpose(Uz)
                Vp[ii,:,:,jj] = Vi[ii,:,:,jj].squeeze() - np.transpose(Vz)
                Tp[ii,:,:,jj] = Ti[ii,:,:,jj].squeeze() - np.transpose(Tz)
        
        print 'Elapsed time: %.2f s' % ((time.time() - t))
        t = time.time()
        print 'Averaging (over %d snapshots)' % len(itc)

        # Average in time and longitude, weighted by surface pressure
        Pvar = Pvar + np.sum(Pp**2, axis = (0,2))
        for jj in range(U.shape[1]):
            Uvar[:,jj] = Uvar[:,jj] + np.transpose(
                np.sum(Psurfi*Up[:,jj,:,:].squeeze()**2, axis = (0,2)))
            Vvar[:,jj] = Vvar[:,jj] + np.transpose(
                np.sum(Psurfi*Vp[:,jj,:,:].squeeze()**2, axis = (0,2)))
            Tvar[:,jj] = Tvar[:,jj] + np.transpose(
                np.sum(Psurfi*Tp[:,jj,:,:].squeeze()**2, axis = (0,2)))
            EKE[:,jj] = EKE[:,jj] + np.transpose(
                np.sum(Psurfi*(
                    Up[:,jj,:,:].squeeze()**2 + 
                    Vp[:,jj,:,:].squeeze()**2), 
                axis = (0,2)))
            EPTF[:,jj] = EPTF[:,jj] + np.transpose(
                np.sum(Psurfi*Vp[:,jj,:,:].squeeze()*Tp[:,jj,:,:].squeeze(), 
                    axis = (0,2)))

    # END for kk in range(nr)
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Final averaging'

    # Finish averaging eddy quantities
    Pvar = Pvar/(len(iters)*Pp.shape[2])
    for jj in range(U.shape[1]):
        Uvar[:,jj] = Uvar[:,jj]/Psurfz
        Vvar[:,jj] = Vvar[:,jj]/Psurfz
        Tvar[:,jj] = Tvar[:,jj]/Psurfz
        EKE[:,jj] = EKE[:,jj]/Psurfz
        EPTF[:,jj] = EPTF[:,jj]/Psurfz
    Psurfz = Psurfz/(len(iters)*Psurfi.shape[2])
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))
    t = time.time()
    print 'Saving'
    
    # Load vertical coordinate
    sigma = mds.rdmds('%s/RC' % params['ddir'])/rosurf

    # Save fields
    f = h5py.File(fname, 'w')

    f['theta'] = Tz
    f['theta'].attrs['units'] = 'K'
    f['theta'].attrs['longname'] = 'Potential Temperature'
    
    f['sigma'] = sigma.squeeze()
    f['sigma'].attrs['units'] = 'nondim'
    f['sigma'].attrs['longname'] = 'sigma'

    f['lat'] = yci[:,0].squeeze()
    f['lat'].attrs['units'] = 'degrees'
    f['lat'].attrs['longname'] = 'latitude'

    f['theta'].dims.create_scale(f['lat'], 'lat')
    f['theta'].dims[0].attach_scale(f['lat'])
    f['theta'].dims[0].label = 'lat'
    f['theta'].dims.create_scale(f['sigma'], 'sigma')
    f['theta'].dims[1].attach_scale(f['sigma'])
    f['theta'].dims[1].label = 'sigma'

    f['ps'] = Psurfz
    f['ps'].attrs['units'] = 'Pa'
    f['ps'].attrs['longname'] = 'Zonal Mean Surface Pressure'
    f['ps'].dims.create_scale(f['lat'], 'lat')
    f['ps'].dims[0].attach_scale(f['lat'])
    f['ps'].dims[0].label = 'lat'
    
    f['psvar'] = Pvar
    f['psvar'].attrs['units'] = 'Pa^2'
    f['psvar'].attrs['longname'] = 'Zonal Mean Surface Pressure Variance'
    f['psvar'].dims.create_scale(f['lat'], 'lat')
    f['psvar'].dims[0].attach_scale(f['lat'])
    f['psvar'].dims[0].label = 'lat'

    f['u'] = Uz
    f['u'].attrs['units'] = 'm/s'
    f['u'].attrs['longname'] = 'Zonal Wind'
    f['u'].dims.create_scale(f['lat'], 'lat')
    f['u'].dims[0].attach_scale(f['lat'])
    f['u'].dims[0].label = 'lat'
    f['u'].dims.create_scale(f['sigma'], 'sigma')
    f['u'].dims[1].attach_scale(f['sigma'])
    f['u'].dims[1].label = 'sigma'
    
    f['v'] = Vz
    f['v'].attrs['units'] = 'm/s'
    f['v'].attrs['longname'] = 'Meridional Wind'
    f['v'].dims.create_scale(f['lat'], 'lat')
    f['v'].dims[0].attach_scale(f['lat'])
    f['v'].dims[0].label = 'lat'
    f['v'].dims.create_scale(f['sigma'], 'sigma')
    f['v'].dims[1].attach_scale(f['sigma'])
    f['v'].dims[1].label = 'sigma'
    
    f['thetavar'] = Tvar
    f['thetavar'].attrs['units'] = 'K^2'
    f['thetavar'].attrs['longname'] = 'Potential Temperature Variance'
    f['thetavar'].dims.create_scale(f['lat'], 'lat')
    f['thetavar'].dims[0].attach_scale(f['lat'])
    f['thetavar'].dims[0].label = 'lat'
    f['thetavar'].dims.create_scale(f['sigma'], 'sigma')
    f['thetavar'].dims[1].attach_scale(f['sigma'])
    f['thetavar'].dims[1].label = 'sigma'
    
    f['uvar'] = Uvar
    f['uvar'].attrs['units'] = 'm^2/s^2'
    f['uvar'].attrs['longname'] = 'Zonal Wind Variance'
    f['uvar'].dims.create_scale(f['lat'], 'lat')
    f['uvar'].dims[0].attach_scale(f['lat'])
    f['uvar'].dims[0].label = 'lat'
    f['uvar'].dims.create_scale(f['sigma'], 'sigma')
    f['uvar'].dims[1].attach_scale(f['sigma'])
    f['uvar'].dims[1].label = 'sigma'
    
    f['vvar'] = Vvar
    f['vvar'].attrs['units'] = 'm^2/s^2'
    f['vvar'].attrs['longname'] = 'Meridional Wind Variance'
    f['vvar'].dims.create_scale(f['lat'], 'lat')
    f['vvar'].dims[0].attach_scale(f['lat'])
    f['vvar'].dims[0].label = 'lat'
    f['vvar'].dims.create_scale(f['sigma'], 'sigma')
    f['vvar'].dims[1].attach_scale(f['sigma'])
    f['vvar'].dims[1].label = 'sigma'
    
    f['EKE'] = EKE
    f['EKE'].attrs['units'] = 'm^2/s^2'
    f['EKE'].attrs['longname'] = 'Eddy Kinetic Energy'
    f['EKE'].dims.create_scale(f['lat'], 'lat')
    f['EKE'].dims[0].attach_scale(f['lat'])
    f['EKE'].dims[0].label = 'lat'
    f['EKE'].dims.create_scale(f['sigma'], 'sigma')
    f['EKE'].dims[1].attach_scale(f['sigma'])
    f['EKE'].dims[1].label = 'sigma'
    
    f['EPTF'] = EPTF
    f['EPTF'].attrs['units'] = 'K*m/s'
    f['EPTF'].attrs['longname'] = 'Eddy Potential Temperature Flux'
    f['EPTF'].dims.create_scale(f['lat'], 'lat')
    f['EPTF'].dims[0].attach_scale(f['lat'])
    f['EPTF'].dims[0].label = 'lat'
    f['EPTF'].dims.create_scale(f['sigma'], 'sigma')
    f['EPTF'].dims[1].attach_scale(f['sigma'])
    f['EPTF'].dims[1].label = 'sigma'
    
    f.close()
    
    print 'Elapsed time: %.2f s' % ((time.time() - t))   
    print 'Leaving MITGCM_eddies.cs_zonal_eddy_uvT'
    print


def cs_zonal_diag(fname, params):
    """
    Compute zonal mean of some diagnostic
    from diagnostic output.

    Parameters
    ----------
    fname: string
        Name of the output file where the zonal mean is stored
        The output file format will be NetCDF

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'longname': string
            Descriptive name of diagnostic variable
        'units': string
            Units of variable
        'dname': int
            Diagnostics file name
        'recnum': string
            Name of the diagnostic field
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
        'xi0': float
            Lowest longitude coordinate
        'xiN': float
            Highest longitude coordinate
        'nx': int
            Number of longitude points
        'yi0': float
            Lowest latitude coordinate
        'yiN': float
            Highest latitude coordinate
        'ny': int
            Number of latitude points
        'p0': float
            Reference surface pressure for sigma coordinates
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Construct grid
    xi = np.linspace(float(params['xi0']), float(params['xiN']),
            int(params['nx']))
    yi = np.linspace(float(params['yi0']), float(params['yiN']),
            int(params['ny']))
    xi, yi = np.meshgrid(xi, yi)

    # Get diagnostic metadata
    dfields = diag_fields(params['ddir'], params['dname'])
    key = dfields[int(params['recnum'])]

    # Compute zonal mean
    D = streaming.zonal_nanmean_ll(
            (lambda i: interpolator_diag( i,
                                    params['ddir'],
                                    params['dname'],
                                    int(params['recnum']),
                                    xi, yi
                                  )),
            its = iters).transpose()
    sigma = mds.rdmds('%s/RC' % params['ddir'])/float(params['p0'])

    # Save
    f = h5py.File(fname, 'w')
    f[key] = D
    f[key].attrs['units'] = params['units']
    f[key].attrs['longname'] = params['longname']
    
    f['sigma'] = sigma.squeeze()
    f['sigma'].attrs['units'] = 'nondim'
    f['sigma'].attrs['longname'] = 'sigma'

    f['lat'] = yi[:,0].squeeze()
    f['lat'].attrs['units'] = 'degrees'
    f['lat'].attrs['longname'] = 'latitude'

    f[key].dims.create_scale(f['lat'], 'lat')
    f[key].dims[0].attach_scale(f['lat'])
    f[key].dims[0].label = 'lat'
    f[key].dims.create_scale(f['sigma'], 'sigma')
    f[key].dims[1].attach_scale(f['sigma'])
    f[key].dims[1].label = 'sigma'

    f.close()

def total_mass(fname, params):
    """
    Compute a timeseries of the total atmospheric mass

    Parameters
    ----------
    fname: string
        Name of the output file where the timseries is stored
        The output file format will be HDF5

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Compute total mass
    mtot = np.zeros(iters.size)
    ddir = params['ddir']
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    rac = mds.rdmds('%s/RAC' % ddir, returnmeta = False)
    drf = mds.rdmds('%s/DRF' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)
    ii = 0
    for it in iters:
        # Get surface pressure (stored in Eta at end)
        Eta = mds.rdmds('%s/Eta' % (ddir), itrs = it, returnmeta = False)
        Eta = Eta + rosurf

        # Convert to mass and integrate over area
        mtot[ii] = np.sum(Eta*rac)/9.80
        ii+=1

    # Save
    f = h5py.File(fname, 'w')
    f['mass'] = mtot
    f['mass'].attrs['units'] = 'kg'
    f['mass'].attrs['longname'] = 'Total atmospheric mass'
    
    f['iter'] = iters
    f['iter'].attrs['units'] = 'nondim'
    f['iter'].attrs['longname'] = 'Iteration'

    f['mass'].dims.create_scale(f['iter'], 'iter')
    f['mass'].dims[0].attach_scale(f['iter'])
    f['mass'].dims[0].label = 'iter'

    f.close()

def dry_mass(fname, params):
    """
    Compute a timeseries of the dry atmospheric mass

    Parameters
    ----------
    fname: string
        Name of the output file where the timseries is stored
        The output file format will be HDF5

    params: dict
        Dictionary of function options. Every dictionary value should
        be a string, and the types here indicate the types that the
        strings must be parseable to. Required keys are
        'iter0': int
            First iteration
        'iterN': int
            Last iteration
        'iterS': int
            Step between iterations
        'ddir': string
            Directory containing output
    """
    # Construct list of iterations
    iters = np.arange(int(params['iter0']), int(params['iterN']),
            int(params['iterS']))

    # Compute dry mass
    mtot = np.zeros(iters.size)
    ddir = params['ddir']
    xc = mds.rdmds('%s/XC' % ddir, returnmeta = False)
    yc = mds.rdmds('%s/YC' % ddir, returnmeta = False)
    rac = mds.rdmds('%s/RAC' % ddir, returnmeta = False)
    drf = mds.rdmds('%s/DRF' % ddir, returnmeta = False)
    rosurf = mds.rdmds('%s/topo_P' % ddir, returnmeta = False)
    ii = 0
    for it in iters:
        # Get surface pressure (stored in Eta at end)
        Eta = mds.rdmds('%s/Eta' % (ddir), itrs = it, returnmeta = False)
        Eta = Eta + rosurf
        
        # Compute fraction of mass in each column that is moist
        q = mds.rdmds('%s/S' % ddir, itrs = it, returnmeta = False)
        for jj in range(q.shape[0]):
            q[jj,:,:] = q[jj,:,:]*drf[jj]
        q = np.sum(q, axis=0)/np.sum(drf)
        
        # Convert to mass and integrate over area
        mtot[ii] = np.sum((1.-q)*Eta*rac)/9.80
        ii+=1

    # Save
    f = h5py.File(fname, 'w')
    f['mass'] = mtot
    f['mass'].attrs['units'] = 'kg'
    f['mass'].attrs['longname'] = 'Dry atmospheric mass'
    
    f['iter'] = iters
    f['iter'].attrs['units'] = 'nondim'
    f['iter'].attrs['longname'] = 'Iteration'

    f['mass'].dims.create_scale(f['iter'], 'iter')
    f['mass'].dims[0].attach_scale(f['iter'])
    f['mass'].dims[0].label = 'iter'

    f.close()
