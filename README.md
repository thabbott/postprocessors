# PostProcessors
Python tools for automating postprocessing tasks. Primarily intended for simple analyses of simulations (e.g. computation of zonal means, etc.) but could be adapted for other uses.

These tools use a few different classes: PostProcessor, which runs analysis functions and writes output files (one per class instance), and Launcher, which reads from a configuration file and determines which PostProcessors to launch.

## Using the tool

1. Create a configuration file containing the tasks you want to run.
2. Create a Launcher, call its setup method, and call its launch method
3. The Launcher will intelligently determine which postprocessing tasks need to be run and run them.

## Using with SLURM swarms
To use a slurm swarm to launch the postprocessing jobs, set up a Launcher, call its swarm method to create a swarm file (PostProcessing.swarm) and a set of python scripts (in a hidden directory with a random name) and then use the swarm file to launch a swarm.

## Configuration file syntax:

For each postprocessing task, include a block in PostProcessors.config delimited by lines starting with "!start" and "!end" (case-insensitive). Inside the block, include, in order, the name of the analysis function, the name of the analysis output file, and a list of analysis parameters in key:value format. Example:

    !START
    zonal_mean
    zonal_mean.nc
    field: EKE
    start_iter: 0
    end_iter: 240000
    step: 240
    !END

You can also include comments on lines starting with "#" and blank lines. An example configuration file is included in this repository.
