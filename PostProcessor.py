import PPConstants
import os
import importlib
import traceback

class PostProcessor(object):

    def __init__(self, func, fname, params):
        """
        Parameters
        ----------
        func: function
            The analysis function run by this postprocessor. Must take
            a string and a dictionary as arguments, as it is called with
            fname and params as parameters.
        fname: string
            The file to which the analysis function writes output
        params: dict
            A dictionary of additional analysis function parameters
        """
        self.func = func
        self.fname = fname
        self.params = params
        self.status = PPConstants.NEVER_STARTED

    def run(self):
        """"
        Run the postprocessor provided that a file with name fname does
        not exist

        Returns
        -------
        int: function return code. See PPConstants for details.
        """
        try:
            if os.path.exists(self.fname):
                self.status = PPConstants.FILE_EXISTS
            else:
                mod_name, func_name = self.func.rsplit('.',1)
                mod = importlib.import_module(mod_name)
                func = getattr(mod, func_name)
                func(self.fname, self.params)
                self.status = PPConstants.SUCCESS
        except Exception, e:
            self.status = PPConstants.FAIL
            with open(PPConstants.ERROR_LOG, 'a') as errs:
                errs.write("%s\n" % self.func)
                errs.write("%s\n" % str(e))
                traceback.print_exc(file = errs)


