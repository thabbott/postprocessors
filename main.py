"""
Example postprocessor script
"""

from Launcher import Launcher
import MITGCM

# Create a launcher
l = Launcher()

# Set up tasks
l.setup()

# Print status before running
print "Status before running"
l.status_report()
print

# Run
l.launch()

# Print status after running
print "Status after running"
l.status_report()
print
