"""
Constants used by the PostProcessor tools
"""

# Status codes
NEVER_STARTED = -1
SUCCESS = 0
FAIL = 1
FILE_EXISTS = 2

# Configuration file name
CONFIG = "PostProcessors.config"
ERROR_LOG = "PostProcessors.errors"

# Configuring file parsing
EOF = ''
COMMENT = '#'
BLOCK_START = '!start'
BLOCK_END = '!end'
BLANK_LINE = '\n'
